unit BitCheckBox;

interface

uses
  SysUtils, Classes, Controls, Graphics, StdCtrls, Vcl.Buttons;

type
  TBitCheckBox = class(TCustomControl)
  private
    FChecked: boolean;
    FImagelist: TImagelist;
    FTransparentSet, FMultiCheck: boolean;
    FPosition: integer;
    FCaption: TCaption;
    procedure SetChecked(Check: boolean);
    procedure SetCaption(Value: TCaption);
    function GetChecked: boolean;
    procedure Imagelistwrite(Images: TImagelist);
    function Imagelistread: TImagelist;
    procedure SetPosition(const Value: integer);
    procedure SetMultiCheck(const Value: boolean);
  protected
    procedure Paint; override;
    procedure Click; override;
    function GetEnabled: boolean; override;
    procedure SetEnabled(Value: boolean); override;
    procedure Repaint; override;
    procedure PaintCheckBox;
    procedure PaintMultCheck;
    procedure PaintCheckBoxNoImage;
    procedure PaintMultiCheckNoImage;

  public
    constructor Create(AOwner: TComponent); virtual;
    destructor Destroy; override;
  published
    property ImageList: TImagelist read Imagelistread write Imagelistwrite;
    property OnClick;
    property Checked: boolean read GetChecked write SetChecked;
    property Color; // default $00404040;
    property Enabled read GetEnabled write SetEnabled;
    property Caption read FCaption write SetCaption;
    property Font;
    property MultiCheck: boolean read FMultiCheck write SetMultiCheck;
    property Position: integer read FPosition write SetPosition;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Dinmaite', [TBitCheckBox]);
end;

{ TBitCheckBox }

procedure TBitCheckBox.Click;
begin
  inherited;
  FChecked := not FChecked;
  Repaint;
end;

constructor TBitCheckBox.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

end;

destructor TBitCheckBox.Destroy;
begin
  inherited;
end;

function TBitCheckBox.GetChecked: boolean;
begin
  Result := FChecked;
end;

function TBitCheckBox.GetEnabled: boolean;
begin
  Result := inherited GetEnabled;
end;

function TBitCheckBox.Imagelistread: TImagelist;
begin
  Result := FImagelist;
end;

procedure TBitCheckBox.Imagelistwrite(Images: TImagelist);
begin
  FImagelist := Images;
end;

procedure TBitCheckBox.Paint;
begin
  inherited Paint;
  canvas.Brush.Color := Color;
  canvas.Font := Font;
  if Caption = '' then
    Caption := Name;

  If FImagelist <> nil then
    begin
      if FMultiCheck then
        PaintMultCheck
      else
        PaintCheckBox;
      Height := FImagelist.Height;
      Width := FImagelist.Width + canvas.TextWidth(FCaption) + 12;
      canvas.TextOut(Width + 6, canvas.TextHeight(FCaption) div 20, FCaption);
    end
  else
    begin
      if FMultiCheck then
        PaintMultiCheckNoImage
      else
        PaintCheckBoxNoImage;
      Height := canvas.TextHeight(FCaption) + 2;
      Width := canvas.TextWidth(FCaption) + Height + 12;
      canvas.TextOut(Height + 6, canvas.TextHeight(FCaption) div 20, FCaption);
    end;
end;

procedure TBitCheckBox.PaintCheckBox;
begin
  if not Enabled then
    FImagelist.Draw(canvas, 0, 0, 2)
  else
    if Checked then
      FImagelist.Draw(canvas, 0, 0, 1)
    else
      FImagelist.Draw(canvas, 0, 0, 0);
end;

procedure TBitCheckBox.PaintCheckBoxNoImage;
var
  BrushColorBuf, PenColorBuf: TColor;
begin
  BrushColorBuf := canvas.Brush.Color;
  PenColorBuf := canvas.Pen.Color;
  if not Enabled then
    begin
      canvas.Brush.Color := clGray;
      canvas.Pen.Color := clGray;
      canvas.FillRect(Rect(0, 0, Height, Height));
    end
  else
    if Checked then
      begin
        canvas.Brush.Color := clGreen;
        canvas.Pen.Color := clGreen;
        canvas.FillRect(Rect(0, 0, Height, Height));
      end
    else
      begin
        canvas.Brush.Color := clRed;
        canvas.Pen.Color := clRed;
        canvas.FillRect(Rect(0, 0, Height, Height));
      end;
  canvas.Brush.Color := BrushColorBuf;
  canvas.Pen.Color := PenColorBuf;
end;

procedure TBitCheckBox.PaintMultCheck;
begin
  if not Enabled then
    FImagelist.Draw(canvas, 0, 0, 2)
  else
    FImagelist.Draw(canvas, 0, 0, FPosition);
end;

procedure TBitCheckBox.PaintMultiCheckNoImage;
var
  Text: string;
  PenColorBuf: TColor;
begin
  PenColorBuf := canvas.Pen.Color;
  if Enabled then
    canvas.Pen.Color := clBlack
  else
    canvas.Pen.Color := clGray;
  canvas.Rectangle(Rect(0, 0, Height, Height));
  Text := IntToStr(FPosition);
  canvas.TextOut((Height div 2) - (canvas.TextWidth(Text) div 2),
    (Height div 2) - (canvas.TextHeight(Text) div 2), Text);
  canvas.Pen.Color := PenColorBuf;
end;

procedure TBitCheckBox.Repaint;
begin
  inherited Repaint;
end;

procedure TBitCheckBox.SetCaption(Value: TCaption);
begin
  FCaption := Value;
end;

procedure TBitCheckBox.SetChecked(Check: boolean);
begin
  FChecked := Check;
  Repaint;
end;

procedure TBitCheckBox.SetEnabled(Value: boolean);
begin
  inherited SetEnabled(Value);
  if Value then
    Font.Color := clWindowText
  else
    Font.Color := clGrayText;
  Repaint;
end;

procedure TBitCheckBox.SetMultiCheck(const Value: boolean);
begin
  FMultiCheck := Value;
  Repaint;
end;

procedure TBitCheckBox.SetPosition(const Value: integer);
begin
  if Value = - 1 then
    inc(FPosition)
  else
    FPosition := Value;
  Repaint;
end;

end.
